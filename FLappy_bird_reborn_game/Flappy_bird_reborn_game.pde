import processing.serial.*;  //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>//
Serial myPort;  

//
// tracks hand movement
//
float Height = 0;
float OldHeight = 0;
float incDec = 0;


//
//pipe X Positions
//
float pipeNum[] = {1300, 1800, 2300};



//
// pipe Gap and Gap height(gate)
//
float gapNum[] = {random(250, 300), random(250, 300), random(250, 300)};
float gateNum[] = {random(10, 550), random(10, 550), random(10, 550)};


//
// score variables
int score     = 0;
int HighScore = 0;
int speed = 100;
boolean pause = true;



void setup() {
  size(1300, 700);
  frameRate(speed);

  // selects serial port
  String portName = Serial.list()[0]; 
  myPort = new Serial(this, portName, 115200);
}


void draw() {


  //
  // pauses game
  //
  if (pause) {
    fill(0);
    textSize(20);
    text("Press any key to Play!", 600, 350);
    return;
  }



  //
  // Calls functions that draw pipes and bird
  //
  background(255);
  for (int i = 0; i < 3; i++) {
    pipeR();
  }   
  bird(track()); 


  //
  // Displays score
  //
  fill(0);
  textSize(20);
  text("HighScore: " + HighScore + "   Score: " + score, 1000, 35);
  
  
  //
  // Increaces speed of game if highscore is passed
  //
    if (score > HighScore) {
    speed = HighScore + 200;
    frameRate(speed);
  }
}


//
// collects values from controller over serial
//
float track() {
  println("-----------Track----------");

  String inString = "000";
  myPort.write("r");    // requests new value

  //
  // if new value recieved then convert it to a float
  //
  if ( myPort.available() > 0) {  
    inString = myPort.readStringUntil('\n');
    Height = float(inString);
    println("Height: " + Height);
  }

  return Height;
}


//
// Makes Pipe
//
void pipe(float x, float gate, float gap) {
  fill(23, 160, 28);    
  rect(x, 700-gate, 100, 1000);
  rect(x, 700-gap-gate, 100, -1000);
}


//
// Makes pipes size/ position random and score system
// also checks for a collision
//
void pipeR() {
  println("---------pipeR----------");

  for (int i = 0; i < 3; i++) {
    pipeNum[i] --;               // moves current pipe to the left
    pipe(pipeNum[i], gateNum[i], gapNum[i]);    // draws current pipe

    //
    // adds 1 to score or resets game
    //
    if (pipeNum[i] == 200) { 
      score++;

      checkCollision(gapNum[i], gateNum[i]);
    } 


    //
    // when pipe exits left side of screen, 
    // place it on the right side of screen
    //
    if (pipeNum[i] == - 100) {
      gapNum[i]  = random(250, 300);
      gateNum[i] = random(10, 550);
      pipeNum[i] = 1400;
    }
  }
}



//
// checks if the bird hit a pipe 
//
void checkCollision(float gap, float gate) {

  if (Height < gate || Height > gap+gate) {
    println("bT3 hit");
    reset();
  } else {
    println("----------CLEARED----------");
  }
}



//
// resets all vairables to their begining state
// restarts game
//
void reset() {
  println("----------RESET-----------");

  if (score > HighScore) {
    HighScore = score;
  }
  score = 0;

  pipeNum[0] = 1300;
  pipeNum[1] = 1800;
  pipeNum[2] = 2300;

  for (int i = 0; i < 3; i++) {
    gapNum[i]  = random(250, 300);
    gateNum[i] = random(10, 550);
  }
  speed = 100;
  frameRate(speed);
  pause = true;
  delay(2000);
}



//
// draws bird on screen
//
void bird(float birdY) {
  println("-------BIRD------------");

  birdY = constrain(birdY, 0, 700);    //keeps bird on screen
  noStroke();
  fill(242, 255, 72);


  // draws bird
  triangle(200, 650-birdY, 250, 675-birdY, 200, 700-birdY);
}

void keyPressed() {
  pause = !pause;
}