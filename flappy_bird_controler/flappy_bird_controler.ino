// ---------------------------------------------------------------------------
// Example NewPing library sketch that does a ping about 20 times per second.
// ---------------------------------------------------------------------------

String inputString = "";
boolean stringComplete = false;

#include <NewPing.h>

#define TRIGGER_PIN  12  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN     11  // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE 56 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

void setup() {
  Serial.begin(115200); // Open serial monitor at 115200 baud to see ping results.
}


void loop() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    inputString += inChar;
  }

  if (inputString == "r") {
  //  delay(50);
    int val = map(sonar.ping_cm(), 0, 56, 0, 750);
    Serial.println(val);
  }
  inputString = "";
}






